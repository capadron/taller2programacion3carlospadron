#Taller 2 Carlos Padron 8-761-1436 U. Latina SEDE Chite.
#listas de provincias con sus distritos.
BOCAS_DEL_TORO = ["Bocas del Toro", "Changuinola", "Chiriquí Grande", "Almirante"]
COCLÉ = ["Aguadulce", "Antón", "La Pintada", "Natá", "Olá", "Penonomé"]
COLÓN = ["Colón", "Chagres", "Donoso", "Portobelo", "Santa Isabel", "Omar Torrijos Herrera"]
CHIRIQUÍ = ["Alanje", "Barú", "Boquerón", "Boquete", "Bugaba", "David", "Dolega", "Gualaca", "Remedios", "Renacimiento",
            "San Félix", "Tierras Altas", "San Lorenzo", "Tolé"]
DARIÉN = ["Chepigana", "Pinogana", "Santa Fe"]
HERRERA = ["Chitré", "Las Minas", "Los Pozos", "Ocú", "Parita", "Santa María", "Pesé"]
LOS_SANTOS = ["Guararé", "Las Tablas", "Los Santos", "Macaracas", "Pedasí", "Pocrí", "Tonosí"]
PANAMÁ = ["Balboa", "Chimán", "Chepo", "Taboga", "Panamá", "San Miguelito"]
VERAGUAS = ["Atalaya", "Calobre", "Cañazas", "La Mesa", "Las Palmas", "Mariato", "Montijo", "Río de Jesús",
            "San Francisco", "Santa Fe", "Santiago", "Soná"]
Guna_Yala = []
COMARCA_EMBERÁ_WOUNAAN = ["Cémaco" , "Sambú"]
COMARCA_NGÄBE_BUGLÉ = ["Besiko", "Jirondai", "Kankintú", "Kusapín", "Mironó", "Muná", "Nole Duima", "Ñurum",
                       "Santa Catalina o Calovébora"]
PANAMÁ_OESTE = ["Arraiján", "Capira", "Chame", "San Carlos", "La Chorrera"]
Madugandí = []
Wargandí = []
#para estandarizar todos las provincias a minusculas para limitar los errores de escritura, dandole un valor numerico.
provincias = {"bocas del toro": "1", "coclé": "2", "colón": "3", "chiriquí": "4", "darién": "5", "herrera": "6",
              "los santos": "7", "panamá": "8", "veraguas": "9", "guna yala": "10", "comarca emberá-wounaan": "11",
              "comarca ngäbe buglé": "12", "panamá oeste": "13", "madugandí": "14", "wargandí": "15"}
#usando valor numerico anterior para limitar errores en la busqueda en el disccinario.
Listado = {"1" : BOCAS_DEL_TORO,"2" : COCLÉ,"3" : COLÓN,"4" : CHIRIQUÍ,"5" : DARIÉN,"6" : HERRERA,"7" : LOS_SANTOS,
           "8" : PANAMÁ,"9" : VERAGUAS,"10" : Guna_Yala,"11" : COMARCA_EMBERÁ_WOUNAAN,"12" : COMARCA_NGÄBE_BUGLÉ,
           "13" : PANAMÁ_OESTE,"14" : Madugandí,"15" : Wargandí}


print ("""BOCAS DEL TORO con el numero 1.
COCLÉ con el numero 2.
COLÓN con el numero 3.
CHIRIQUÍ con el numero 4.
DARIÉN con el numero 5.
HERRERA con el numero 6.
LOS SANTOS con el numero 7.
PANAMÁ con el numero 8.
VERAGUAS con el numero 9.
Guna Yala con el numero 10.
COMARCA EMBERÁ-WOUNAAN con el numero 11.
COMARCA NGÄBE BUGLÉ con el numero 12.
PANAMÁ OESTE con el numero 13.
Madugandí con el numero 14.
Wargandí con el numero 15.
Opcion 0 para Salir""")
temp = 1
while temp != 0: #loop infinito hasta que seleccione la opcion de salir.
    temp = input ("Introdusca el Nombre de la Provincia o su Numeracion: ")
    if temp.isdigit(): #saber si el valor introduccido es numerico. para usar la el diccionario correcto.
        if temp == "0": #La opcion de Salida.
            exit()
        else:
            try: #Capturador de error
                print(Listado[temp]) #imprecion de la lista.
            except: #error capturado, intenar nuevamente. usando input como una pausa.
                temperror = input("Opcion incorrecta, presione enter para intentarlo nuevamente.")
    else:#si el valor no es numerico
        temp_lower = temp.lower() #convertir valor a minuscula para estandarizar la busqueda.
        temp2 = provincias[temp_lower] #la busqueda ya estandarizada para conseguir le valor numerico
        try: #Capturador de error
            print(Listado[temp2])
        except:
            temperror = input("Opcion incorrecta, presione enter para intentarlo nuevamente.")
